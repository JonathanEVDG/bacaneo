/*Variables*/
Alloy.Globals.jsonImage = null;
/*Metodos*/


/*Método para crear el dialogo*/
var crearDialogo = function(){
  var opts = {
  	cancel: 2,
  	options: ['Camara', 'Galeria', 'Cancelar'],
  	title: 'Escoger opción'
  };

  Alloy.Globals.dialog = Ti.UI.createOptionDialog(opts);

  Alloy.Globals.dialog.addEventListener("click",function(e){
  	if (e.index === 0)tomarFoto();
    else if (e.index === 1)loadFile();
  });
}

/*Método para tomar desde la camara*/
var tomarFoto = function(){

  // Carga la camara.
  var cameraTransform = Ti.UI.create2DMatrix();
  cameraTransform = cameraTransform.scale(1);

  Titanium.Media.showCamera({
    success:function(event){
      var image = event.media.imageAsResized(event.width*0.2,event.height*0.2);
      var mimeType = event.media.mimeType;
      if(OS_ANDROID){
        var imageName = event.media.file.name;
        var path = event.media.file.nativePath;
      }
      var filehandle = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, imageName);
      filehandle.write(image);

      path = true === OS_ANDROID? path : filehandle.nativePath;

      Alloy.Globals.jsonImage = {
        image      : image,
        mimeType   : mimeType,
        imageName  : imageName,
        path       : path,
        filehandle : filehandle
      };

      OS_ANDROID?
        $.imageViewFotoEscogida.image = filehandle:
        $.imageViewFotoEscogida.image = image;
		},
    cancel:function(){
        console.log('cancel');
    },
    error:function(error){
      //error happend, create alert
      var a = Titanium.UI.createAlertDialog({title:'Camera'});
      //set message
      if (error.code == Titanium.Media.NO_CAMERA){
          a.setMessage('el dispositivo no tiene camara');
      }else{
          a.setMessage('Unexpected error: ' + error.code);
      }
      // show alert
      a.show();
    },
     autohide:true,
     allowEditing:true,
     showControls:true,
     mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO],
	});
}
/*Cargar archivo desde el sistema de archivos*/
var loadFile = function(){

  Titanium.Media.openPhotoGallery({
    allowEditing:true,
    success:function(event){
      var image = event.media;
      var mimeType = event.media.mimeType;
      if(OS_ANDROID){
        var imageName = event.media.file.name;
        var path = event.media.file.nativePath;
      }
      var filehandle = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, imageName);
      filehandle.write(image);
      console.log(filehandle.imageName);
      console.log(filehandle.nativePath);
      if(OS_ANDROID){
        Alloy.Globals.jsonImage = {
          image      : image,
          mimeType   : mimeType,
          imageName  : imageName,
          path       : path,
          filehandle : filehandle
        };
      }else {
        Alloy.Globals.jsonImage = {
          image      : image,
          mimeType   : mimeType,
          imageName  : imageName,
          path       : filehandle.nativePath,
          filehandle : filehandle
        };
      }
      OS_ANDROID?
        $.imageViewFotoEscogida.image = filehandle:
        $.imageViewFotoEscogida.image = image;
    },
    cancel:function(){
      console.log('cancel');
    }
  });
}

/*Método para crear el evento*/
var crearEvento = function(){
  var jsonImage = Alloy.Globals.jsonImage,
      nombre = $.textFieldNombreEvento.getValue(),
      calificacion = $.textFieldCalificacion.getValue(),
      descripcion = $.textAreaDescripcion.getValue();

  // if (null != jsonImage ) {
  if (null != jsonImage && '' != nombre && '' != calificacion && '' != descripcion) {
    var eventoText = 'Nombre: '+nombre+'\n'+
                     'Descripción: '+descripcion+'\n'+
                     'Calificación: '+calificacion;
    require('com.alcoapps.socialshare').share({
		status 					    : eventoText,
		image 					    : jsonImage.path,
		androidDialogTitle 	: 'Compartir evento',
	})
  }else {
    var msg = 'Te faltarón estos datos:\n';
    null === jsonImage ? msg+='* No escogiste una imagen.\n':msg+='';
    '' === nombre ? msg+='* No escribiste un nombre para el evento.\n':msg+='';
    '' === calificacion ? msg+='* No escribiste una calificación para el evento.\n':msg+='';
    '' === descripcion? msg+='* No escribiste una descripción para el evento.\n':msg+='';

    alert(msg);
  }

}

/*Listeners*/
$.viewCrearEvento.addEventListener('click',crearEvento);

$.viewCargarFoto.addEventListener('click',function(){
  Alloy.Globals.dialog.show();
});

$.ventana.addEventListener('open',function(){
  crearDialogo();
});

/*Listener para cerrar la ventana*/
if(!OS_IOS){
  $.viewAtras.addEventListener('click',function(){
    $.ventana.close();
  });
}
