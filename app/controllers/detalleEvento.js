/*variables*/
Alloy.Globals.audioPlayer = null;

/*Métodos*/
/*Métodos de ayuda*/
var notificar = function(contentTitle,contentText,tickerText){

	/*Creo el intent*/
	var intent = Titanium.Android.createIntent({
	    action: Titanium.Android.ACTION_MAIN,//Asigno al intent la actividad
	    className: 'co.com.bacaneo.BacaneoActivity',//Le defino al intent el className
	    packageName: 'com.evomedia.gos' //packageName de la apliación
	});

	/*Agrego al intent la categoria para iniciar la app*/
	intent.addCategory(Titanium.Android.CATEGORY_LAUNCHER);

	var pending = Titanium.Android.createPendingIntent({
	    activity: Titanium.Android.currentActivity,
	    intent: intent,
	    type: Titanium.Android.PENDING_INTENT_FOR_ACTIVITY,
	    flags: Titanium.Android.FLAG_ACTIVITY_NEW_TASK
	});

	/*Creo la notificaión*/
	var notification = Titanium.Android.createNotification({
	    contentIntent: pending,
	    contentTitle: contentTitle,
	    contentText: contentText,
	    tickerText: tickerText,
	    icon: Ti.App.Android.R.drawable.appicon,
	    when: new Date().getTime()
	});

	/*Genero la vibración*/
	Titanium.Media.vibrate([0, 1000, 100, 800, 100, 1000]);

	/*Disparo la notificación*/
	Ti.Android.NotificationManager.notify(1, notification);
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/*Métodos principales*/
/*Método para cargar un evento desde la base de datos local o desde la colección remota*/
var cargarEvento = function(){
  if (false === Alloy.Globals.esRemeto) {
    /*Instancio la colección que hace referencia a la tabla en la BdeD*/
    var eventos = Alloy.createCollection('informacionEventos');

    /*Hago la lectura de la tabla realizando un query*/
    eventos.fetch({query:'SELECT * FROM informacionEventos WHERE id='+Alloy.Globals.idEvento});

    _.each(eventos.models,function(item){
      item = item.toJSON();
      $.labelNombreEvento.setText(item.nombre);
      $.ivImagenEvento.setImage(item.url);
      $.labelDescripcionEvento.setText(item.descripcion);
      $.labelCalificacion.setText('Calificacción: '+item.calificacion);
      $.labelId.setText('Id: '+item.id);
    });
  }else{
    var eventosRest = Alloy.createCollection('eventos');
    eventosRest.config.URL = eventosRest.config.URL+'/'+Alloy.Globals.idEvento;
    eventosRest.fetch({
      success:function(response){
        response = response.toJSON()[0];
        console.log(response.Titulo);
        $.labelNombreEvento.setText(response.Titulo);
        $.ivImagenEvento.setImage('http://zoommedellin.co/'+response.ImagenNw);
        $.labelDescripcionEvento.setText(response.Intro);
        $.labelCalificacion.setText('Calificacción: '+response.Calificacion);
        $.labelId.setText('Id: '+response.ID);
      },
      error:function(error){
        console.log(JSON.stringify(error));
      },
    });
    eventosRest.config.URL = 'http://zoommedellin.co/api/Evento';
  }
  var url = [
    'http://evoluciondigital.com.co/bataneo/assets/sound/JohnLegend1.mp3',
    'http://evoluciondigital.com.co/bataneo/assets/sound/Gostan1.mp3',
    'http://evoluciondigital.com.co/bataneo/assets/sound/Scheinizzl1.mp3',
    'http://evoluciondigital.com.co/bataneo/assets/sound/SnoopDogg&KidCudi1.mp3'
  ];
  Alloy.Globals.audioPlayer = Ti.Media.createAudioPlayer({
    url: url[getRandomInt(0,3)],
    allowBackground: true
  });

}

/*Listeners*/
/*Listener para cerrar la ventana*/
if(!OS_IOS){
  $.viewAtras.addEventListener('click',function(){
    $.ventana.close();
  });
}

/*Listener para eliminar un momdelo de la colección*/
$.viewEliminar.addEventListener('click',function(){
  if (false === Alloy.Globals.esRemeto) {
    var eventos = Alloy.createCollection('informacionEventos');
    eventos.deleteById(Alloy.Globals.idEvento);
    notificar('Evento','Un evento ha sido eliminado','Un evento ha sido eliminado');
    $.ventana.close();
  }else {
    alert('Este es un evento remoto no puede ser borrado');
  }
});

/*Listener al evento focus*/
$.ventana.addEventListener('focus',function(){
  cargarEvento();
});

/*Listeners a los iconos play,pause,stop*/
$.labelPlay.addEventListener('click',function(){
  Alloy.Globals.audioPlayer.start();
});

$.labelPause.addEventListener('click',function(){
  Alloy.Globals.audioPlayer.pause();
});

$.labelStop.addEventListener('click',function(){
  Alloy.Globals.audioPlayer.stop();
  if (OS_ANDROID)Alloy.Globals.audioPlayer.release();
});
