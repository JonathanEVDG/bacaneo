var grid = require('lib/gridlines');
// grid.drawgrid(20,$.ventana);

var cargarListView = function(){
  var eventos = Alloy.createCollection('informacionEventos');
  eventos.fetch();

  var data = [];
  /*recorro el arreglo con la información y construyo el objeto con los datos para el listview*/
  _.each(eventos.models,function(item){
    item = item.toJSON();
    var listItem = {
      imagen:{image:item.url},
      evento:{text:item.nombre},
      descripcion:{text:item.descripcion},
      id:{text:item.id},
    };
    data.push(listItem);
  });

  /*Cargo la sección del listview con los datos*/
  $.section.setItems(data);
}

/*Listeners*/

/*Agrego al evento itemclick un listener para intervenir cuando den click a la flecha*/
$.listViewBuscar.addEventListener('itemclick',function(e){
  if ('flecha' === e.bindId) {
    /*Almaceno en una variable global el id de sqlite del evento*/
    Alloy.Globals.idEvento = e.section.items[e.itemIndex].id.text;
    Alloy.Globals.esRemeto = false;
    /*Abro la ventana para ver el detalle del evento*/
    Alloy.Globals.paginar('detalleEvento');
  }
});

/*Listener para cerrar la ventana*/
if(!OS_IOS){
  $.viewAtras.addEventListener('click',function(){
    $.ventana.close();
  });
}

/*Listener para cargar los datos en los listview*/
$.ventana.addEventListener('focus',cargarListView);
