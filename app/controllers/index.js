var grid = require('lib/gridlines');
// grid.drawgrid(20,$.ventana);

// Declaracin de variables y metodos globales
Alloy.Globals.nav = $.nav;//Variable para controlar la navegación en ios
Alloy.Globals.jsonGps = {};

/*Funciones pricipales*/
function doClickSMS(e) {
    Ti.Platform.openURL('sms:+12345678901');
}

function doClickDialer(e) {
    Ti.Platform.openURL('tel://408-555-1234');
}

function doClickYouTube(e) {
    Ti.Platform.openURL('http://www.youtube.com/watch?v=iodEa0eitag');
}

/*Método para Compartir por redes sociales*/
function shareText(e){
 // share text status
 require('com.alcoapps.socialshare').share({
 		status 				      : 'Te recomiendo está super aplicación',
 		androidDialogTitle 	: 'Compartir Bataneo',
 	});
}

/*Método para constuir los datos para el listview y cargarlos*/
var buildDataListView = function(section,array){
  var data = [];
  /*recorro el arreglo con la información y construyo el objeto con los datos para el listview*/
  _.each(array,function(item){
    if(item.ImagenNw){
      var listItem = {
        imagen:{image:'http://zoommedellin.co/'+item.ImagenNw},
        evento:{text:item.Titulo},
        descripcion:{text:item.Intro},
        id:{text:item.ID},
      };
      data.push(listItem);
    }
  });

  /*Cargo la sección del listview con los datos*/
  section.setItems(data);
}

/*FUnción para consultar los eventos*/
var getData = function(){
  var seccion1 = [],seccion2 = [],seccion3 = [];

  /*Instancio la coleccion remota*/
  var eventosRest = Alloy.createCollection('eventos');

  /*Hago la petición a la coleccion*/
  eventosRest.fetch({
  	success:function(response){
  		response = response.toJSON();

      /*Recorro la respuesta y clasifico los eventos al arreglo correspondiente*/
  		_.each(response,function(item){
  			if (1 === item.Seccion)seccion1.push(item);
  			if (2 === item.Seccion)seccion2.push(item);
  			if (3 === item.Seccion)seccion3.push(item);
  		});

  		buildDataListView($.cuerpo.sectionCategoria1,seccion1);
  		buildDataListView($.cuerpo.sectionCategoria2,seccion2);
  		buildDataListView($.cuerpo.sectionCategoria3,seccion3);
  	},
  	error:function(){
  		console.log('Hubo un error');
  	}
  });
}

/*Agrego al evento itemclick un listener para intervenir cuando den click a la flecha en el listview*/
$.cuerpo.listViewCategoria1.addEventListener('itemclick',function(e){
  if ('flecha' === e.bindId) {
    /*Almaceno en una variable global el id de sqlite del evento*/
    Alloy.Globals.idEvento = e.section.items[e.itemIndex].id.text;
    Alloy.Globals.esRemeto = true;
    /*Abro la ventana para ver el detalle del evento*/
    Alloy.Globals.paginar('detalleEvento');
  }
});

$.cuerpo.listViewCategoria2.addEventListener('itemclick',function(e){
  if ('flecha' === e.bindId) {
    /*Almaceno en una variable global el id de sqlite del evento*/
    Alloy.Globals.idEvento = e.section.items[e.itemIndex].id.text;
    Alloy.Globals.esRemeto = true;
    /*Abro la ventana para ver el detalle del evento*/
    Alloy.Globals.paginar('detalleEvento');
  }
});

$.cuerpo.listViewCategoria3.addEventListener('itemclick',function(e){
  if ('flecha' === e.bindId) {
    /*Almaceno en una variable global el id de sqlite del evento*/
    Alloy.Globals.idEvento = e.section.items[e.itemIndex].id.text;
    Alloy.Globals.esRemeto = true;
    /*Abro la ventana para ver el detalle del evento*/
    Alloy.Globals.paginar('detalleEvento');
  }
});

/*Listener al icono del mundo*/
$.cuerpo.labelUbicacion.addEventListener('click',function(){
  Alloy.Globals.paginar('ubicacion');
});

/*Listener a la red*/
Ti.Network.addEventListener('change',function(e){

	/*Declaro una variable para acceder al icono de datos*/
	var datos	= $.cuerpo.labelDatos;

	/*Valido si está online*/
	false === e.online ? datos.setColor('red') : datos.setColor('green');
});

/*Listener al evento open*/
$.ventana.addEventListener('open',function(){
  getData();
  var gps = $.cuerpo.labelGps,
      datos	= $.cuerpo.labelDatos;

  false == Ti.Geolocation.getLocationServicesEnabled()?
    gps.setColor('red'):
    gps.setColor('green');

  Ti.Network.networkType == Ti.Network.NETWORK_NONE?
    datos.setColor('red'):
    datos.setColor('green');

});

/*Listeners para abrir secciones*/
// Listener para abrir la sección buscar
$.cuerpo.labelBuscar.addEventListener('click',function(){
  Alloy.Globals.paginar('buscar');
});

// Listener para abrir la sección crear evento
$.cuerpo.labelAquiSuena.addEventListener('click',function(){
  Alloy.Globals.paginar('crearEvento');
});

// Listener para abrir la sección buscar
$.cuerpo.labelInformacion.addEventListener('click',function(){
  Alloy.Globals.paginar('informacion');
});
/*--*/

// LIstener para abrir el menu lateral
$.cuerpo.labelMenu.addEventListener('click',function(){
  Alloy.Globals.animarMenu($.viewCuerpo);
});

/*Listeners a los elementos del menu lateral*/
// Listener para el boton de inicio
$.menu.inicio.addEventListener('click',function(){
  Alloy.Globals.animarMenu($.viewCuerpo);
});

// Listener para el boton de compratir
$.menu.compartir.addEventListener('click',function(){
  Alloy.Globals.animarMenu($.viewCuerpo);
  shareText();
  // $.wCompartir.mostrar();//Hago visible el listado de redes sociales
});

// Listener para el boton de explorar
$.menu.explorarEventos.addEventListener('click',function(){
  Alloy.Globals.animarMenu($.viewCuerpo);
  doClickYouTube();
});

// Listener para el botón escribenos
$.menu.escribenos.addEventListener('click',function(){
  Alloy.Globals.animarMenu($.viewCuerpo);
	var emailDialog = Ti.UI.createEmailDialog()
	emailDialog.toRecipients = ['vicmancb@gmail.com'];
	emailDialog.subject = 'Comentarios';
	emailDialog.messageBody = '';
	emailDialog.open();
});

if(OS_IOS){
	Alloy.Globals.nav.open();
}else{
	$.ventana.open();
};
