// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
Alloy.Globals.toogleMenu = 1;

Alloy.Globals.animarMenu = function(view, direccion){
	var objAnimacion = Ti.UI.createAnimation();
	objAnimacion.duration = 350;
	if((Alloy.Globals.toogleMenu === 0 && direccion !== "left") || direccion === "right"){
		objAnimacion.left=0;
		Alloy.Globals.toogleMenu = 1;
	}else{
		objAnimacion.left="300";
		Alloy.Globals.toogleMenu = 0;
	}
	view.animate(objAnimacion);
	objAnimacion = null;
};

Alloy.Globals.paginar=function(pagina){
	Alloy.Globals.seccion = Alloy.createController(pagina).getView();
	if(OS_IOS){
		Alloy.Globals.nav.openWindow(Alloy.Globals.seccion, {animated: true});
	}else{
		Alloy.Globals.seccion.open();
	}
};


/*Método para obtener la ubicación*/
var obtenerPosicion = function(){
	try{
		// Activo modo manual:
		Ti.Geolocation.Android.manualMode = true;
		// Configuración de geolocalizcaion cuando manualMode esta OFF.
		Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;

		// Registro de origenes de datos.
		var	gpsProvider = Ti.Geolocation.Android.createLocationProvider({
			name: Ti.Geolocation.PROVIDER_GPS,
			minUpdateTime: 60,
			minUpdateDistance: 100
		});
    if (OS_ANDROID){
      Ti.Geolocation.Android.addLocationProvider(gpsProvider);
  		// Reglas de localización.
  		var gpsRule = Ti.Geolocation.Android.createLocationRule({
  			provider: Ti.Geolocation.PROVIDER_GPS,
  			// Updates should be accurate to 100m
  			accuracy: 100,
  			// Updates should be no older than 30 seconds
  			maxAge: 60000,
  			// But  no more frequent than once per 10 seconds
  			minAge: 10000
  		});
	    Ti.Geolocation.Android.addLocationRule(gpsRule);
    }
		// posicion actaul
		Ti.Geolocation.addEventListener('location', function(e){
			e.cancelBubble = true;
      if (true === e.success) {
        Alloy.Globals.jsonGps = {
          latitude : e.coords.latitude,
          longitude : e.coords.longitude
        }
      }else {
				console.log(e.source.lastGeolocation.latitude);
        Alloy.Globals.jsonGps = {
          latitude : e.source.lastGeolocation.latitude,
          longitude : e.source.lastGeolocation.longitude,
        }
      }
      console.log('alloy.js:80 - '+JSON.stringify(Alloy.Globals.jsonGps));
    });
	}catch (error){
		console.log(error);
	}
}

obtenerPosicion();

// var eventos = Alloy.createCollection('informacionEventos');
// eventos.create({
// 	nombre				: 'los muppets',
// 	descripcion		: 'los muppets en concierto este mes de noviembre en plaza mayor, con todo su elenco',
// 	url						: 'http://content.cuspide.com/getcover.ashx?ISBN=9789871409501&size=3&coverNumber=1&id_com=1113',
// 	calificacion	: Math.floor((Math.random() * 5) + 1)
// });
