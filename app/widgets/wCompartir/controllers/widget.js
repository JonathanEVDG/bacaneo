/*función para mostrar el widget*/
function mostrar (){
  $.ventana.show();
}

/*Listener para cerrar le ventana desde el icono cerrar*/
$.LabelIconoCerrar.addEventListener('click',function(){
  $.ventana.hide();
});

/*Listener para cerrar le ventana al dar click por fuera*/
$.ventana.addEventListener('click',function(){
  $.ventana.hide();
});

exports.mostrar = mostrar;
