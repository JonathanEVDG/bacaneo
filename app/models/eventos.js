exports.definition = {
    config : {
        "URL"   : "http://zoommedellin.co/api/Evento",
        "debug": 0,
        "timeout":10000,
        "adapter": {
            "type": "restapi",
            "collection_name": "eventos"
        },
        "headers": { // your custom headers

        },
        // "parentNode": "" //your root node
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};
