exports.definition = {
    config : {
        "URL"   : "http://104.197.11.169:3000/api/bataneos/update?where={\"id\":\"5643d4572e52129a73f32a47\"}",
        "debug": 1,
        "timeout":10000,
        "adapter": {
            "type": "restapi",
            "collection_name": "bataneos"
        },
        "headers": { // your custom headers

        },
        // "parentNode": "" //your root node
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};
