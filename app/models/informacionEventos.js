exports.definition = {
	config : {
		// Config.
		"columns" : {
			"id"				    : "INTEGER PRIMARY KEY AUTOINCREMENT",
			"nombre"				: "VARCHAR",
			"descripcion"		: "VARCHAR",
			"url"			      : "VARCHAR",
			"calificacion"	: "INTEGER"
		},
		"defaults" : {

		},
		"adapter" : {
			"type"				    : "sql",
			"collection_name"	: "informacionEventos",
			"db_file"			    : "/db/EventosDb.sqlite",
			"db_name"			    : "EventosDb",
			"idAttribute"	   	: "id"
		}
	},

	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// Modelos.
		});

		return Model;
	},

	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
      deleteById : function(id) {

        var collection = this;

        var sql = "DELETE FROM " + collection.config.adapter.collection_name +" WHERE id="+id;
        db = Ti.Database.open(collection.config.adapter.db_name);
        db.execute(sql);
        db.close();

        collection.trigger('sync');

      }
		});

		return Collection;
	}
};
